#include <stdlib.h>
#include "exec-all.h"
#include "tcg-op.h"
#include "helper.h"
#define GEN_HELPER 1
#include "helper.h"
#include "optimization.h"

/* HW2 Using Macros */
#define OPT_SHADOW_STACK
#define OPT_IBTC

extern uint8_t *optimization_ret_addr;

/* shack_init: allocate space to shack */
static inline void shack_init(CPUState *env)
{
#ifdef OPT_SHADOW_STACK
	/* Orz... begin-> top -> ... end. */
        env->shack_top = (void *)malloc(SHACK_SIZE * sizeof(void *));
	env->shack     = env->shack_top;

        env->shack_end = env->shack + SHACK_SIZE * sizeof(void *);
        env->shadow_hash_list =
            (struct shack_hash *)calloc(1, sizeof(struct shack_hash));
#endif
}

/* search_hash_table():
 *	search the hash table by using the hash key. 		 
 */
struct shadow_pair *search_hash_table(CPUState *env, target_ulong next_eip)
{
        size_t hash_key = next_eip & SHACK_MASK;
        struct shadow_pair *find_pair = env->shadow_hash_list->begin[hash_key];

        while (find_pair != NULL) {
                if (find_pair->guest_eip == next_eip)
                        return find_pair;
                find_pair = find_pair->next_entry;
        }

        struct shadow_pair *new_pair =
            (struct shadow_pair *)malloc(sizeof(struct shadow_pair));
        new_pair->guest_eip  = next_eip;
        new_pair->host_eip   = NULL;
        new_pair->next_entry = env->shadow_hash_list->begin[hash_key];
        env->shadow_hash_list->begin[hash_key] = new_pair;

        return new_pair;
}

/* shack_set_shadow():
 *      update the host return address
 */
void shack_set_shadow(CPUState *env, target_ulong guest_eip,
                      unsigned long *host_eip)
{
#ifdef OPT_SHADOW_STACK
	search_hash_table(env, guest_eip)->host_eip = host_eip;
#endif
}

/* helper_shack_flush():
 * flush the shack by modify the pointer
 */
void helper_shack_flush(CPUState *env)
{
#ifdef OPT_SHADOW_STACK
        env->shack_top = env->shack;
#endif
}

void push_shack(CPUState *env, TCGv_ptr cpu_env, target_ulong next_eip)
{
#ifdef OPT_SHADOW_STACK
        TCGv_ptr temp_shack_top, temp_shack_end;
        temp_shack_top = tcg_temp_new_ptr();
        temp_shack_end = tcg_temp_new_ptr();

        int push_label = gen_new_label();

        struct shadow_pair *pair_ptr = search_hash_table(env, next_eip);

	/* check if the table need to flush or not */ 
        tcg_gen_ld_ptr(temp_shack_top, cpu_env, offsetof(CPUState, shack_top));
        tcg_gen_ld_ptr(temp_shack_end, cpu_env, offsetof(CPUState, shack_end));

	// if (env->shack_top == env->shack_end) 
        tcg_gen_brcond_i32(TCG_COND_NE, temp_shack_top, temp_shack_end,
                           push_label);
	// shack_top = shack;
        tcg_gen_ld_ptr(temp_shack_top, cpu_env, offsetof(CPUState, shack));
        tcg_gen_st_tl(temp_shack_top, cpu_env, offsetof(CPUState, shack_top));

        gen_set_label(push_label);
	tcg_gen_ld_ptr(temp_shack_top, cpu_env, offsetof(CPUState, shack_top));

	// push the target address into shack table        
	// insert into shack_top.
        tcg_gen_st_tl(tcg_const_ptr(pair_ptr), temp_shack_top, 0);
        tcg_gen_addi_ptr(temp_shack_top, temp_shack_top, sizeof(void *));
        tcg_gen_st_tl(temp_shack_top, cpu_env, offsetof(CPUState, shack_top));

        tcg_temp_free_ptr(temp_shack_top);
        tcg_temp_free_ptr(temp_shack_end);
#endif
}

void pop_shack(TCGv_ptr cpu_env, TCGv next_eip)
{
#ifdef OPT_SHADOW_STACK
        TCGv_ptr temp_shack_top 	= tcg_temp_new_ptr();
        TCGv_ptr temp_shack_head 	= tcg_temp_new_ptr();
        TCGv temp_guest_eip_on_top 	= tcg_temp_new();
        TCGv_ptr temp_host_eip_on_top 	= tcg_temp_local_new_ptr();

        TCGv temp_next_eip = tcg_temp_local_new();

        int pop_label = gen_new_label();

        tcg_gen_mov_tl(temp_next_eip, next_eip);

	// check the shack table empty or not.
        tcg_gen_ld_ptr(temp_shack_top, cpu_env, offsetof(CPUState, shack_top));
        tcg_gen_ld_ptr(temp_shack_head, cpu_env, offsetof(CPUState, shack));
        tcg_gen_brcond_i32(TCG_COND_EQ, temp_shack_top, temp_shack_head,
                           pop_label);

	// if not empty, update the shack top entry.
        tcg_gen_ld_ptr(temp_shack_top, cpu_env, offsetof(CPUState, shack_top));
        tcg_gen_subi_i32(temp_shack_top, temp_shack_top, sizeof(void *));
        tcg_gen_st_tl(temp_shack_top, cpu_env, offsetof(CPUState, shack_top));

	// check guest_eip of top == next_eip ? 
        tcg_gen_ld_ptr(temp_shack_top, temp_shack_top, 0);
	// next one ,shack_top--;
        tcg_gen_ld_tl(temp_guest_eip_on_top, temp_shack_top,
                      offsetof(struct shadow_pair, guest_eip));
	// guest_eip of top != next_eip, jump to pop_label.
        tcg_gen_brcond_i32(TCG_COND_NE, temp_guest_eip_on_top, temp_next_eip,
                           pop_label);


	// ! have to check the host_eip on top is vaild or not !!
        tcg_gen_ld_ptr(temp_shack_top, cpu_env, offsetof(CPUState, shack_top));
        tcg_gen_ld_ptr(temp_shack_top, temp_shack_top, 0);
	// compare the host_eip on top and tcg's host eip.
        tcg_gen_ld_ptr(temp_host_eip_on_top, temp_shack_top,
                       offsetof(struct shadow_pair, host_eip));
        tcg_gen_brcond_i32(TCG_COND_EQ, temp_host_eip_on_top,
                           tcg_const_ptr(NULL), pop_label);

	// update the return address, copy from docx.
        *gen_opc_ptr++ 		= INDEX_op_jmp;
	// Orz.. GET_TCGV_I32.
        *gen_opparam_ptr++ 	= GET_TCGV_I32(temp_host_eip_on_top);

        gen_set_label(pop_label);
        tcg_temp_free_ptr(temp_shack_top);
        tcg_temp_free_ptr(temp_shack_head);
        tcg_temp_free_ptr(temp_host_eip_on_top);
        tcg_temp_free(temp_guest_eip_on_top);
        tcg_temp_free(temp_next_eip);
#endif
}

__thread int update_ibtc;
struct ibtc_table *ibtc;
// target_ulong saved_eip;

/* helper_lookup_ibtc():
 *	return next host eip if hit.
 */
void *helper_lookup_ibtc(target_ulong guest_eip)
{
#ifdef OPT_IBTC
        int hash_key = guest_eip & IBTC_CACHE_MASK;
	// Hit.
        if (ibtc->htable[hash_key].guest_eip == guest_eip)
                return (void *)ibtc->htable[hash_key].tb->tc_ptr;

        // saved_eip = guest_eip;
        update_ibtc = 1;
#endif
        return optimization_ret_addr;
}

/* update_ibtc_entry():
 * 	update the entry in htable by using the hash key.
 */
void update_ibtc_entry(TranslationBlock *tb) {
#ifdef OPT_IBTC
        update_ibtc = 0;
	// According the pc is fine. 
        int hash_key = tb->pc & IBTC_CACHE_MASK;
        ibtc->htable[hash_key].guest_eip = tb->pc;
        ibtc->htable[hash_key].tb = tb;
#endif
}

/* ibtc_init():
 * 	allocate the space for ibtc table.
 */
static inline void ibtc_init(CPUState *env) {
#ifdef OPT_IBTC
        ibtc = (struct ibtc_table *)calloc(1, sizeof(struct ibtc_table));
        update_ibtc = 0;
#endif
}

int init_optimizations(CPUState *env) {
        shack_init(env);
        ibtc_init(env);

        return 0;
}
