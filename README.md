## QEMU 0.13.0 Optimization

* This is the NCTU Virtual Machine course work
* Modify QEMU 0.13, in order to support **Shadow stack** and **Indirect Branch Code Cache**.

- - -

- Environment:
    - Ubuntu 14.04 (32bit)
    - gcc 4.9
    - perf

- Files:
    - src/
         - The modified code of QEMU 0.13

- Usage:
    - make build
         - Create build.qemu/ and build.qemu.opt/, second one is the optimization version.

    - make test-branch
         - Running test program in test/

    - make test-benchmark
         - Running MiBench, Coremark programs in benchmark/

    - make clean
         - Clean all files.
- - -
