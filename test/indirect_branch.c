/*
* It is a stupid program which have lots of indirect branch
*/

#include <stdio.h>
#include <stdlib.h>
typedef unsigned long long LLong;

// + and -
LLong r_add_min(int n)
{
	LLong ret = 0;
	int i;
	for (i = 0; i < n; ++i)
	{
		if (i % 2)
			ret -= i;
		ret += i;
	}
	return ret;
}

// * and / 
LLong r_mux_div(int n)
{
	LLong ret = 0;
	int i;
	for (i = 0; i < n; ++i)
	{
		if (i % 2)
			ret /= i;
		ret *= i;
	}
	return ret;
}

int main(int argc, char *argv[])
{
	int n = atoi(argv[1]);
	LLong sum = 0;
	/* use a function pointer point to the functions */
	LLong (*fptr[])(int) = {r_add_min, r_mux_div};
	int i;
	for (i = 1; i <= n; ++i)
	{
		int m = i % 2;
		if (m == 0)
			goto L1;
		else
			goto L2;

L2:
		sum += (*fptr[1])(n);
		continue;
L1:
		sum += (*fptr[0])(n);
	}
	printf("Sum : %lld\n", sum);
}
