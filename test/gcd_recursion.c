/*
* A really stupid gcd implementation.
* In order to waste the time.
*/

#include <stdio.h>
#include <stdlib.h>

int gcd(int n1, int n2)
{

	if (n2 != 0)
		goto gcd1;
	else
		goto gcd2;

gcd1:
	return gcd(n2, n1 % n2);
gcd2:
	return n1;
}

int main(int argc, char *argv[])
{
	int n1 = atoi(argv[1]);
	int n2 = atoi(argv[2]);
	int ret = gcd(n1, n2);
	printf("Gcd of %d and %d = %d\n", n1, n2, ret);
	return 0;
}
