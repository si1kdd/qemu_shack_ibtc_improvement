/*
* It is a tiny programs with simple branch instruction
*/

#include <stdio.h>
#include <stdlib.h>
typedef unsigned long long LLong;

int main(int argc, char *argv[])
{
	int n = atoi(argv[1]);
	LLong sum = 0;

	int i, j;
	for (i = 1; i <= n; ++i)
	{
		int m = i % 2;
		LLong ret = 0;
		if (m == 0) {
			for (j = 0; j < n; ++j)
			{
				if (j % 2)
					ret -= j;
				ret += j;
			}
		}
		if (m == 1) {
			for (j = 0; j < n; ++j)
			{
				if (j % 2)
					ret /= j;
				ret *= j;
			}
		}
		sum += ret;
	}

	printf("Sum : %lld\n", sum);
}
