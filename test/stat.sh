#!/usr/bin/env bash

QEMU_OPT_DIR="../build.qemu.opt/i386-linux-user/qemu-i386"
QEMU_ORI_DIR="../build.qemu/i386-linux-user/qemu-i386"

testcases=("gcd 1212121212 3939393939" "gcd 339933 6565659" "gcd 9582198238 8129357931" 
		"direct 5000" "direct 10000" "direct 20000" "indirect 5000" "indirect 10000" "indirect 20000")

echo -e "\033[47m\033[31m[*]Start testing\033[0m"
echo
echo > ./log.txt

# for i in "${testcases[@]}"; do
	# echo -e "\033[47m\033[31m[*]Native result : ${i} \033[0m" 
	# (time ./${i}) >> ./log.txt 
	# echo
	# (perf stat -o ./result/"Native_$i".txt ./${i}) >> ./log.txt
	# echo "-----------------------------------------------"
# done

for i in "${testcases[@]}"; do
	echo -e "\033[40m\033[33m[*]Qemu : ${i} \033[0m" 
	(time ${QEMU_ORI_DIR} ./${i}) >> ./log.txt
	echo
	(perf stat -o ./result/"QEMU_$i".txt ${QEMU_ORI_DIR} ./${i}) >> ./log.txt
	echo "-----------------------------------------------"
done


for i in "${testcases[@]}"; do
	echo -e "\033[40m\033[34m[*]Qemu Optimization : ${i} \033[0m" 
	(time ${QEMU_OPT_DIR} ./${i}) >> ./log.txt
	echo
	(perf stat -o ./result/"QEMU_OPT_$i".txt ${QEMU_OPT_DIR} ./${i}) >> ./log.txt
	echo "-----------------------------------------------"
done
