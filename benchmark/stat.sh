#!/usr/bin/env bash

QEMU_OPT_DIR="../build.qemu.opt/i386-linux-user/qemu-i386"
QEMU_ORI_DIR="../build.qemu/i386-linux-user/qemu-i386"

bsmall="./basicmath/basicmath_small"
blarge="./basicmath/basicmath_large"
bitcount="bitcount/bicnts 5000000"
qs1="qsort/qsort_small ./qsort/input_small.dat"
qs2="qsort/qsort_large ./qsort/input_large.dat"
core1="coremark_v1.0/coremakr.exe 0x0 0x0 0x66 0 7 1 2000"
core2="coremark_v1.0/coremakr.exe 0x3415 0x3415 0x66 0 7 1 2000"

echo -e "\033[40m\033[1;33m QEMU basicmath_small \033[0m"
perf stat -o ../result/"QEMU_bsmall".txt ${QEMU_ORI_DIR} ${bsmall}
echo -e "\033[40m\033[1;33m QEMU basicmath_large \033[0m"
perf stat -o ../result/"QEMU_blarge".txt ${QEMU_ORI_DIR} ${blarge}
echo -e "\033[40m\033[1;33m QEMU bitcount \033[0m"
perf stat -o ../result/"QEMU_bitcount".txt ${QEMU_ORI_DIR} ${bitcount}
echo -e "\033[40m\033[1;33m QEMU qsort small \033[0m"
perf stat -o ../result/"QEMU_qs1".txt ${QEMU_ORI_DIR} ${qs1}
echo -e "\033[40m\033[1;33m QEMU qsort_large \033[0m"
perf stat -o ../result/"QEMU_qs2".txt ${QEMU_ORI_DIR} ${qs2}
echo -e "\033[40m\033[1;33m QEMU coremark_1.0 0x0 0x0 0x66 0 7 1 2000\033[0m"
perf stat -o ../result/"QEMU_core1".txt ${QEMU_ORI_DIR} ${core1}
echo -e "\033[40m\033[1;33m QEMU coremark_1.0 0x3415 0x3415 0x3415 0x66 0 7 1 200\033[0m"
perf stat -o ../result/"QEMU_core2".txt ${QEMU_ORI_DIR} ${core2}

echo -e "\033[40m\033[1;33m QEMU Optimization basicmath_small \033[0m"
perf stat -o ../result/"QEMU_OPT_bsmall".txt ${QEMU_OPT_DIR} ${bsmall}
echo -e "\033[40m\033[1;33m QEMU Optimization basicmath_large \033[0m"
perf stat -o ../result/"QEMU_OPT_blarge".txt ${QEMU_OPT_DIR} ${blarge}
echo -e "\033[40m\033[1;33m QEMU Optimization bitcount \033[0m"
perf stat -o ../result/"QEMU_OPT_bitcount".txt ${QEMU_OPT_DIR} ${bitcount}
echo -e "\033[40m\033[1;33m QEMU Optimization qsort small \033[0m"
perf stat -o ../result/"QEMU_OPT_qs1".txt ${QEMU_OPT_DIR} ${qs1}
echo -e "\033[40m\033[1;33m QEMU Optimization qsort_large \033[0m"
perf stat -o ../result/"QEMU_OPT_qs2".txt ${QEMU_OPT_DIR} ${qs2}
echo -e "\033[40m\033[1;33m QEMU coremark_1.0 0x0 0x0 0x66 0 7 1 2000\033[0m"
perf stat -o ../result/"QEMU_OPT_core1".txt ${QEMU_OPT_DIR} ${core1}
echo -e "\033[40m\033[1;33m QEMU coremark_1.0 0x3415 0x3415 0x3415 0x66 0 7 1 200\033[0m"
perf stat -o ../result/"QEMU_OPT_core2".txt ${QEMU_OPT_DIR} ${core2}

