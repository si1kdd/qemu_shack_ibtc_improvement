build:
	@echo -e "\033[1;31m[*]Extract the files ! \033[0m"
	tar -xf qemu_virtual_machine.tar.bz2
	mkdir qemu-HW2 && cp -r qemu-0.13.0/* qemu-HW2/
	mkdir build.qemu && mkdir build.qemu.opt
	cd build.qemu && \
		../qemu-0.13.0/configure --target-list=i386-linux-user --disable-docs && \
		make
	cd ..
	@echo -e "[*] Copy src files"
	sleep 2
	cp -r ./src/* qemu-HW2/.
	cd build.qemu.opt && \
		../qemu-HW2/configure --target-list=i386-linux-user --disable-docs && \
		make
	cd ..

test-branch:
	@echo -e "\033[47m\033[34m[*] Running branch test !\033[0m"
	make -C ./test clean
	make -C ./test && make -C ./test run

test-benchmark:
	@echo -e "\033[40m\033[34m[*] Run Benchmark Tools !\033[0m"
	make -C ./benchmark clean
	make -C ./benchmark && make -C ./benchmark run
	make -C ./benchmark run-test

clean:
	@echo -e "\033[40m\033[32m[*] Clean all files !\033[0m"
	@rm -rf qemu-0.13.0 qemu-HW2
	@rm -rf build.qemu build.qemu.opt
	@rm -rf result/*
